import customers from './assets_accounts.js';
import './customer-account.js';

// const templateMain = document.createElement('template');
// templateMain.innerHTML = /*html*/`
//     <h2><slot name="title"><i>[Puudub pealkiri!]</i></slot></h2>
// `;

const templateMain = /*html*/`
    <h2><slot name="title"><i>[Puudub pealkiri!]</i></slot></h2>
    <div id="customers"></div>
`;

// const templateAccount = /*html*/`
//     <customer-account>
//     </customer-account>
// `;

class CustomerAccountList extends HTMLElement {

    constructor() {
        console.log(customers);
        super();
        this._shadow = this.attachShadow({ mode: 'open' });
        // const contentNode = document.importNode(templateMain.content, true);
        // this._shadow.appendChild(contentNode);
        this._shadow.innerHTML = templateMain;
    }

    connectedCallback() {
        const customersDiv = this._shadow.querySelector('#customers');

        customers.forEach(account => {
            const accountElement = document.createElement('customer-account');
            accountElement.accountDetails = account;
            customersDiv.appendChild(accountElement);
        });

        console.log(customersDiv);
    }
}

window.customElements.define('customer-account-list', CustomerAccountList);