
const mainHtml = /*html*/`
    <h3>Kliendiandmed</h3>
    <table>
        <tbody>
            <tr>
                <td>Nimi</td>
                <td id="name"></td>
            </tr>
            <tr>
                <td>Kontonumber</td>
                <td id="accountNumber"></td>
            </tr>
            <tr>
                <td>Kontojääk</td>
                <td id="accountBalance"></td>
            </tr>
        </tbody>
    </table>
`;

class CustomerAccount extends HTMLElement {

    constructor() {
        super();
        this._shadow = this.attachShadow({ mode: 'open' });
        this._shadow.innerHTML = mainHtml;
    }

    connectedCallback() {
        const nameElement = this._shadow.querySelector('#name');
        const accountNumberElement = this._shadow.querySelector('#accountNumber');
        const accountBalanceElement = this._shadow.querySelector('#accountBalance');

        nameElement.textContent = this._accountDetails.firstName + ' ' + this._accountDetails.lastName;
        accountNumberElement.textContent = this._accountDetails.number;
        accountBalanceElement.textContent = this._accountDetails.balance;
    }

    set accountDetails(details) {
        this._accountDetails = details;
    }
}

window.customElements.define('customer-account', CustomerAccount);