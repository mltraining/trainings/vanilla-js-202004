class ChildItem extends HTMLElement {

    static get observedAttributes() {
        return ['name'];
    }

    attributeChangedCallback(name, oldValue, newValue) {
        switch (name) {
            case 'name':
                this.name = newValue;
                break;
        }
    }

    connectedCallback() {
        this.render();
    }

    render() {
        this.innerHTML = /*html*/`            
            <div>
                ${this.name}
            </div>
            <input type="text">
        `;

        this.addEventListener('click', this.sendMessageToParent);
    }

    sendMessageToParent(event) {
        if (event.target.nodeName !== 'INPUT') {
            let input = event.target.querySelector('input');
            let messageText = input.value;
            input.value = '';
            if (messageText != '') {
                let customEvent = new CustomEvent('messagesend', {
                    bubbles: true,
                    detail: {
                        sender: this.name,
                        message: messageText
                    }
                });
                console.log('Sending custom event', customEvent);
                event.target.dispatchEvent(customEvent);
            }
        }
    }

}

window.customElements.define('child-item', ChildItem);