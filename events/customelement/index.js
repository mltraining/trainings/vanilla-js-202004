
window.addEventListener('load', () => {
    let parent = document.querySelector('#parent');
    parent.addEventListener('messagesend', receiveMessageFromChild);
});

function receiveMessageFromChild(event) {
    console.log('Receiving custom event "messagesend"', event);
    let messages = document.querySelector('#messages');
    let message = document.createElement('div');
    message.innerHTML = `<b>${event.detail.sender}</b>: ${event.detail.message}`;
    messages.appendChild(message);
}
