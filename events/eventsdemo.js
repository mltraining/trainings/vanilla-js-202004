
window.addEventListener('load', function () {
    let parent = document.querySelector('#parent');
    let child1 = document.querySelector('#child1');
    let child2 = document.querySelector('#child2');
    let child3 = document.querySelector('#child3');

    parent.addEventListener('messagesend', receiveMessageFromChild);
    child1.addEventListener('click', sendMessageToParent);
    child2.addEventListener('click', sendMessageToParent);
    child3.addEventListener('click', sendMessageToParent);
});

function sendMessageToParent(event) {
    if (event.target.nodeName !== 'INPUT') {
        let input = event.target.querySelector('input');
        let messageText = input.value;
        input.value = '';
        if (messageText != '') {
            let customEvent = new CustomEvent('messagesend', {
                bubbles: true,
                detail: {
                    message: messageText
                }
            });
            console.log('Sending custom event', customEvent);
            event.target.dispatchEvent(customEvent);
        }
    }
}

function receiveMessageFromChild(event) {
    console.log('Receiving custom event "messagesend"', event);
    let messages = document.querySelector('#messages');
    let message = document.createElement('div');
    message.innerHTML = `<b>${event.target.id}</b>: ${event.detail.message}`;
    messages.appendChild(message);
}
