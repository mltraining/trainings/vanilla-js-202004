const BINDING_INFO_REGEX = /(.+(?=\|))?(\|)?(.+(?=\=))?(\=)?(.*)?/;

let models = new Map();

let proxyHandler = {
    get(target, key) {
        if (typeof target[key] === 'object' && target[key] !== null) {
            return new Proxy(target[key], proxyHandler);
        } else {
            return target[key];
        }
    },
    set(target, prop, value) {
        target[prop] = value;
        bindAll();
        return true;
    }
};

window.addEventListener('load', () => {
    bindModel(companies, 'companies');
    bindModel(
        { 
            name: 'Marek Lints', 
            profession: 'Software Developer', 
            contacts: { 
                phone: '+37255902559',
                email: 'marek.lints@gmail.com'
            } 
        }, 'person');
    bindAll();
});

function bindModel(model, modelName) {
    models.set(modelName, new Proxy(model, proxyHandler));
}

function extractBindingInfo(expression) {
    const groups = expression.match(BINDING_INFO_REGEX);
    return {
        expression: expression,
        modelName: groups[1],
        path: groups[5] && groups[5].split('.') || [],
        targetProp: groups[3]
    };
}

function findBoundObject(model, path) {
    let boundObject = model;
    for(const prop of path) {
        boundObject = boundObject[prop];
    }
    return boundObject;
}

function bindIndependentElements(rootElement) {
    let elements = rootElement.querySelectorAll('[data-bind]');
    for (let element of elements) {
        const expression = element.getAttribute('data-bind');
        const bindingInfo = extractBindingInfo(expression);
        const model = models.get(bindingInfo.modelName);
        const boundObject = findBoundObject(model, bindingInfo.path);
        element[bindingInfo.targetProp] = boundObject;
    }
}

function bindRepeatingElements(rootElement) {
    let templates = rootElement.querySelectorAll('[data-bind-array]');
    for (let template of templates) {
        const expression = template.getAttribute('data-bind-array');
        const bindingInfo = extractBindingInfo(expression);
        const model = models.get(bindingInfo.modelName);
        const boundArray = findBoundObject(model, bindingInfo.path);
        for (let boundObject of boundArray) {
            let contentElement = rootElement.importNode(template.content, true);
            contentElement.querySelector("*").setAttribute('data-dynamic-element', '');
            bindSubElements(boundObject, contentElement);
            template.parentNode.appendChild(contentElement);
        }
    }
}

function bindSubElements(model, contentElement) {
    let elements = contentElement.querySelectorAll('[data-bind]');
    for (let element of elements) {
        const expression = element.getAttribute('data-bind');
        const bindingInfo = extractBindingInfo(expression);
        const boundObject = findBoundObject(model, bindingInfo.path);
        element[bindingInfo.targetProp] = boundObject;
    }
}

function clearDynamicElements(rootElement) {
    while (rootElement.querySelector('[data-dynamic-element]')) {
        rootElement.querySelector('[data-dynamic-element]').remove();
    }
}

function bindAll() {
    clearDynamicElements(document);
    bindIndependentElements(document);
    bindRepeatingElements(document);
}