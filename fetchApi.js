const API_URL = 'https://frogaccelerator.com:8110/companies';

// let response = fetch(API_URL);
// response
//     .then(function handleResponse(r) {
//         return r.json();
//     }).then(
//         function handleCompanies(companies) {
//             console.log(companies);
//         }
//     );
// console.log('Fetch method called');

function retrieveCompaniesOldWay() {
    let response = fetch(API_URL);
    response
        .then(function handleResponse(r) {
            return r.json();
        }).then(
            function handleCompanies(companies) {
                console.log(companies);
            }
        ).catch(
            function (error) {
                console.log('Server exploded', error);
            }
        );
    console.log('Fetch method called');
}

// retrieveCompaniesOldWay();

async function retrieveCompanies() {
    try {
        let response = await fetch(API_URL);
        let companies = await response.json();
        console.log('Some cool companies', companies);
    } catch (e) {
        console.log('Problems!!!', e);
    }
}

retrieveCompanies();
// console.log('retrieveCompanies() called');